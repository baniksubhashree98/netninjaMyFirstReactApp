import React, { useState } from "react";
import "./App.css";
import Modal from "./components/Modal";
import Title from "./components/Title";

function App() {
  const [showModal, setShowModal] = useState(false);
  const [showEvents, setShowEvents] = useState(true);
  const [events, setEvents] = useState([
    { title: "mario's birthday bash", id: 1 },
    { title: "bowser's live stream", id: 2 },
    { title: "race on moo moo farm", id: 3 },
  ]);

  const handleClick = (id) => {
    setEvents((prevEvents) => {
      return prevEvents.filter((event) => {
        return event.id !== id;
      });
    });
    console.log(id);
  };
  const handleClose = () => {
    setShowModal(false);
  };
  const handleShow = () => {
    setShowModal(true);
  };
  const subtitle = "All the lastest events in Marioland";
  return (
    <div className="App">
      <Title title="Events in your area" subtitle={subtitle} />
      {!showEvents && (
        <div>
          <button onClick={() => setShowEvents(true)}>show events</button>
        </div>
      )}
      {showEvents && (
        <div>
          <button onClick={() => setShowEvents(false)}>hide events</button>
        </div>
      )}
      {showEvents &&
        events.map((event, index) => (
          <React.Fragment key={event.id}>
            <h4>
              {index} - {event.title}
            </h4>
            <button
              onClick={() => {
                handleClick(event.id);
              }}
            >
              Delete event
            </button>
          </React.Fragment>
        ))}

      {showModal && (
        <Modal handleClose={handleClose}>
          <h2>10% off coupon code!!</h2>
          <p>Use the code NINJA10 at checkout</p>
        </Modal>
      )}
      <div>
        <button onClick={handleShow}>Show Modal</button>
      </div>
    </div>
  );
}

export default App;
